﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            Category category = Category.INFO;
            ConsoleColor color = ConsoleColor.Cyan;
            ConsoleNotification notification = new ConsoleNotification("Mihovil Kovacevic", "Naslov", "Tekst koji je jako bitan",
                new DateTime(2020, 4, 24, 17, 51, 57), category, color);
            NotificationMAnager manager = new NotificationMAnager();
            manager.Display(notification);
        }
    }
}
